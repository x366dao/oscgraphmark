# OscGraphMark

基于Opencv的示波器图像峰值标记API

## Description

使用API前

![使用API前](https://gitlab.com/x366dao/oscgraphmark/-/raw/main/before.png)

使用API后

![使用API后](https://gitlab.com/x366dao/oscgraphmark/-/raw/main/after.png)

## License
MIT
