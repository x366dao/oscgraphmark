#include "ByteUtilities.h"

int ConvertByteToMat(const unsigned char* fileBytes, cv::Mat& fileMat, const int& bytesLenght) 
{
	try
	{
		//从字节数组中读取内存
		std::vector<unsigned char> fileBuf(fileBytes, fileBytes + bytesLenght);
		fileMat = cv::imdecode(cv::Mat(fileBuf), cv::IMREAD_COLOR);
	}
	catch (const std::exception&)
	{
		return -1;
	}
	return 0;
}

int ConvertMatToByte(const cv::Mat& fileMat, unsigned char* fileBytes, int& bytesLenght)
{
	try
	{
		//将矩阵Mat转换成png格式的字节数组
		std::vector<unsigned char> fileBuf;
		std::vector<int> params;
		params.push_back(cv::IMWRITE_PNG_STRATEGY);
		params.push_back(cv::IMWRITE_PNG_STRATEGY_DEFAULT);
		cv::imencode(".png", fileMat, fileBuf, params);

		bytesLenght = static_cast<int>( fileBuf.size());
		std::copy(fileBuf.begin(), fileBuf.end(), fileBytes);
	}
	catch (const std::exception&)
	{
		return -1;
	}
	return 0;
}