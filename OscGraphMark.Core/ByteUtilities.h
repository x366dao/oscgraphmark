#ifndef BYTEUTILITES_H
#define BYTEUTILITES_H

#include "pch.h"
#include "opencv2/opencv.hpp"
#include "opencv2/core/cvdef.h"
#include "opencv2/highgui.hpp"
#include "opencv2/features2d.hpp"

int ConvertByteToMat(const unsigned char *fileBytes, cv::Mat& fileMat, const int& bytesLenght);

int ConvertMatToByte(const cv::Mat& fileMat, unsigned char* fileBytes, int& bytesLenght);

#endif // !BYTEUTILITES_H
