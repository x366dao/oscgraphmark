#include "CalcGraph.h"

float GetDistance(cv::Point2f pt1, cv::Point2f pt2)
{
	return (float)std::sqrt(std::pow(pt2.x - pt1.x, 2) + std::pow(pt2.y - pt1.y, 2));
}
 
cv::Point2f PointRound(cv::Point2f pt)
{
	return cv::Point2f(static_cast<float> (cvRound(pt.x)), static_cast<float>(cvRound(pt.y)));
}