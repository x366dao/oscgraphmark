#ifndef _EXPORTS_H
#define _EXPORTS_H

#include "pch.h"
#include "Analysis/PreprocessGraph.h"
#include "Analysis/DetectGraph.h"
#include "Analysis/MarkGraph.h"
#include "ByteUtilities.h"
#include "opencv2/opencv.hpp"
#include "opencv2/core/cvdef.h"
#include "opencv2/highgui.hpp"
#include "opencv2/features2d.hpp"

#ifdef CPPDYNAMICLINKLIBRARY_EXPORTS
#define SYMBOL_DECLSPEC __declspec(dllexport)
#define DLL_EXPORT      SYMBOL_DECLSPEC
#define SYMBOL_DEF     
#define DLL_IMPORT      SYMBOL_DEF 
#else
#define SYMBOL_DECLSPEC __declspec(dllimport)
#define DLL_EXPORT		SYMBOL_DECLSPEC
#define SYMBOL_DEF		  __declspec(dllimport)
#define DLL_IMPORT		SYMBOL_DEF
#endif

struct FileBuffer
{
	int size;
	unsigned char* pBuffer;
};

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

SYMBOL_DECLSPEC int __stdcall GetGraphMark( 
	_In_  FileBuffer* pInBuffer,
	_Out_ FileBuffer* pOutBuffer,
	_Out_ float* peakMarkVal
);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _EXPORTS_H
