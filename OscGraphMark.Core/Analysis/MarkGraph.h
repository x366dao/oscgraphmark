#ifndef MARKGRAPH_H
#define MARKGRAPH_H

#include "Graph.h"

struct RectDetail
{
	cv::Point2f leftUpper; //左上角
	cv::Point2f leftLower;//左下角
	cv::Point2f rightUpper;//右上角
	cv::Point2f rightLower;//右下角
	cv::Point2f centroid;//中心点
	cv::Point2f centroidUpperLine;//线垂直的中点
	cv::Point2f centroidLowerLine;
	double axisAngle = 0.0; //矩形框长轴的一个角度

};

#define DefineRealDist 12.5
#define DefinePxDist 50
#define PI 3.14159265359

class MarkGraph :
	public Graph
{
public:
	MarkGraph(const cv::Mat& srcMat, const cv::Mat& contoursMat,
		const std::vector<std::vector<cv::Point> >& maxContours);
	~MarkGraph();

	cv::Mat GetMat() override;
	float GetPeakDist();
private:
	cv::Mat _contoursMat;
	std::vector<std::vector<cv::Point> > _maxContours;
	cv::RotatedRect _maxContourRect;
	cv::Point2f     _maxRectPoints[4];
	RectDetail      _maxRectDetail;
	float _markPeakDist;
	/// <summary>
	/// 找最小矩形框
	/// </summary>
	void FindRect();
	/// <summary>
	/// 找出矩形框的信息
	/// </summary>
	void FindRectInfo();
	/// <summary>
	/// 画标记线
	/// </summary>
	void DrawLineMark();
	/// <summary>
	/// 画出峰峰值线
	/// </summary>
	void DrawPeakLine();
};

#endif // !MARKGRAPH_H
