#include "PreprocessGraph.h"

PreprocessGraph::PreprocessGraph(const cv::Mat& srcMat)
{
	_srcMat = srcMat.clone();
}

PreprocessGraph::~PreprocessGraph(){}

cv::Mat PreprocessGraph::GetMat()
{
	RemoveUnlessArea();
	FindBackgroundPoint();

	//对需要处理的图像进行灰度化和二值化
	cv::cvtColor(_srcMat, _srcMat, cv::COLOR_BGR2GRAY);
	cv::equalizeHist(_srcMat, _srcMat);
	BinThreshold(_srcMat, _srcMat, 1, 255);

	RemoveBackground();
	_dstMat = _srcMat.clone();
	return _dstMat;
}


void PreprocessGraph::RemoveUnlessArea()
{
	//先把四角多余的东西去除掉
	cv::rectangle(_srcMat, cv::Point(0, 0), cv::Point(800, 40), cv::Scalar(0, 0, 0), -1, 4);
	cv::rectangle(_srcMat, cv::Point(0, 0), cv::Point(25, 480), cv::Scalar(0, 0, 0), -1, 4);
	cv::rectangle(_srcMat, cv::Point(800, 0), cv::Point(760, 800), cv::Scalar(0, 0, 0), -1, 4);
	cv::rectangle(_srcMat, cv::Point(0, 415), cv::Point(800, 480), cv::Scalar(0, 0, 0), -1, 4);

	cv::rectangle(_srcMat, cv::Point(508, 40), cv::Point(760, 100), cv::Scalar(0, 0, 0), -1, 4);
	cv::rectangle(_srcMat, cv::Point(0, 0), cv::Point(800, 105), cv::Scalar(0, 0, 0), -1, 4);
}

void PreprocessGraph::FindBackgroundPoint()
{
	//单独提取图片中的R通道,并且得到里面的格子图位置
	std::vector<cv::Mat> multiChannel;
	cv::split(_srcMat, multiChannel);

	_rChannelImage = multiChannel[R_CHANNEL];

	std::vector<cv::Point> backgroundPoint;
	//一整行 y row == heigh == Point.y
	for (int row = 0; row < _rChannelImage.rows; row++)
	{
		//一整列 x col == width == Point.x
		for (int col = 0; col < _rChannelImage.cols; col++)
		{
			//Mat::at(Point(x, y)) == Mat::at(y,x)
			if (_rChannelImage.at<uchar>(cv::Point(col, row)) > 0)
			{

				// cv::Point 是y,x
				backgroundPoint.push_back(cv::Point(col, row));
			}
		}
	}
	_backgroundPoint = backgroundPoint;
}

void PreprocessGraph::BinThreshold(const cv::Mat& srcMat, cv::Mat& dstMat, int threshold, int dstVal)
{
	//cv::cvtColor(_srcMat, _dstMat, cv::COLOR_BGR2GRAY);
	cv::threshold(srcMat, dstMat, threshold, dstVal, cv::THRESH_BINARY);
}

void PreprocessGraph::RemoveBackground()
{
	//cv::absdiff(_dstMat, grayBackground, _dstMat);
	for (std::vector<cv::Point>::iterator iter = _backgroundPoint.begin(); iter != _backgroundPoint.end(); iter++)
	{
		if (_srcMat.channels() == 1)
			_srcMat.at<uchar>(*iter) = 0;
	}
}

