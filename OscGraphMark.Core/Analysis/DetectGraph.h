#ifndef DETECTGRAPH_H
#define DETECTGRAPH_H

#include "Graph.h"

struct VertialPixel
{
	int pixelVertical = 0;
	int pixelCount = 0;
};

class DetectGraph
	:Graph
{
public:
	/// <summary>
	/// 寻找波形所在位置
	/// </summary>
	/// <param name="srcMat">传入单通道图形</param>
	DetectGraph(const cv::Mat& srcMat);
	~DetectGraph();

	
	cv::Mat GetMat() override;
	int GetLeftMark();
	int GetRightMark();
	std::vector<std::vector<cv::Point> > GetTargetContours();
	
private:
	int _leftMark = 0;
	int _rightMark = 0;
	std::vector<std::vector<cv::Point> > _maxContours;
	void FindMarkLine();
	void ProcessMat();
	void FindContour();
};

#endif // !DETECTGRAPH_H
