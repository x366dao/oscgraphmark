#include "MarkGraph.h"

MarkGraph::MarkGraph(const cv::Mat& srcMat, const cv::Mat& contoursMat,
	const std::vector<std::vector<cv::Point> >& maxContours)
{
	_srcMat = srcMat.clone();
	_contoursMat = contoursMat.clone();
	_maxContours = maxContours;
}

MarkGraph::~MarkGraph()
{
}

cv::Mat MarkGraph::GetMat()
{
	FindRect();
	FindRectInfo();
	DrawPeakLine();
	DrawLineMark();
	_dstMat = _srcMat.clone();
	return _dstMat;
}

float MarkGraph::GetPeakDist()
{
	return _markPeakDist;
}

void MarkGraph::FindRect()
{
    _maxContourRect = cv::minAreaRect(cv::Mat(_maxContours[0]));
    //把最小外接矩形的四个端点复制到rect数组
    _maxContourRect.points(_maxRectPoints);
    //中心
    _maxRectDetail.centroid = cv::Point2f(_maxContourRect.center.x, _maxContourRect.center.y);
    //角度
    _maxRectDetail.axisAngle = _maxContourRect.angle;

    //测试
    /*for (int i = 0; i < 4; i++)
    {
        cv::circle(srcImage, _maxRectPoints[i], 2 + i * 2, cv::Scalar(0, 0, 255));
    }*/
    //for (int i = 0; i < 12; i++)
    //{
    //    cv::rectangle(srcImage, cv::Point(0, 0), cv::Point(800, 480), cv::Scalar(0, 0, 0), -1, 4);
    //    cv::RotatedRect test (cv::Point2f(100, 100), cv::Size2f(20, 10), i*30);
    //    cv::Point2f rect[4];
    //    test.points(rect);
    //    for (int j = 0; j < 4; j++)
    //    {
    //        cv::circle(srcImage, rect[j], 2 + j *2, cv::Scalar(0, 0, 255));
    //    }
    //    for (int j = 0; j < 4; j++)
    //    {
    //        cv::line(srcImage, rect[j], rect[(j + 1) % 4], cv::Scalar(0, 0, 255), 1, cv::LINE_AA, 0);  //绘制最小外接矩形每条边
    //    }
    //}
    // 
    //测试

    //判断矩形的4个坐标对应的情况
    // 使用勾股定理算出到原点的距离，长度最小的为左顶点
    //如果出现等长，那么就是斜率最大的为左顶点
    double dist[4] = {};
    for (int i = 0; i < sizeof(dist) / sizeof(double); i++)
    {
        dist[i] = GetDistance(cv::Point2f(0, 0), _maxRectPoints[i]);
    }

    //找距离(0,0)最短距离
    double minDist = dist[0];
    int minDistIndex = 0;
    for (int i = 0; i < sizeof(dist) / sizeof(double); i++)
    {
        if (dist[i] < minDist)
        {
            minDist = dist[i];
            minDistIndex = i;
        }
        else if (dist[i] == minDist)
        {
            double d1 = _maxRectPoints[i].y / _maxRectPoints[i].x;
            double d2 = _maxRectPoints[minDistIndex].y / _maxRectPoints[minDistIndex].x;
            if (d1 < d2)
            {
                minDist = dist[i];
                minDistIndex = i;
            }
        }
    }
    int ij = 0;
    for (int i = minDistIndex; i < sizeof(_maxRectPoints) / sizeof(cv::Point2f); i++)
    {

        switch (ij)
        {
        case 0:_maxRectDetail.leftUpper = _maxRectPoints[i];

            break;
        case 1:_maxRectDetail.rightUpper = _maxRectPoints[i];
            break;
        case 2: _maxRectDetail.rightLower = _maxRectPoints[i];
            break;
        case 3: _maxRectDetail.leftLower = _maxRectPoints[i];
            break;
        default:
            break;
        }
        ij++;
    }
    //给剩下的赋值
    for (int i = 0; i < 4 - ij; i++)
    {
        switch (ij)
        {
        case 0:_maxRectDetail.leftUpper = _maxRectPoints[i];
            break;
        case 1:_maxRectDetail.rightUpper = _maxRectPoints[i];
            break;
        case 2: _maxRectDetail.rightLower = _maxRectPoints[i];
            break;
        case 3: _maxRectDetail.leftLower = _maxRectPoints[i];
            break;
        default:
            break;
        }
        ij++;
    }
}

void MarkGraph::FindRectInfo()
{
	    ////垂足\中点确认
		//_maxRectDetail.centroidUpperLine = cv::Point2f((_maxRectDetail.rightUpper.x + _maxRectDetail.leftUpper.x) / 2,
		//    (_maxRectDetail.rightUpper.y + _maxRectDetail.leftUpper.y) / 2);

		//_maxRectDetail.centroidLowerLine = cv::Point2f((_maxRectDetail.rightLower.x + _maxRectDetail.leftLower.x) / 2,
		//    (_maxRectDetail.rightLower.y + _maxRectDetail.leftLower.y) / 2);

		//要把点延长，只延长较短的线
		//+----------------------+
		//| y=(y2-y1)(x-x1)      |
		//|   -----------  +y1   |
		//|       x2-x1          |
		//|                      |
		//+----------------------+
	if (_maxRectDetail.leftUpper.x > _maxRectDetail.leftLower.x)
	{
		_maxRectDetail.leftUpper.x = _maxRectDetail.leftLower.x;
		_maxRectDetail.leftUpper.y = (_maxRectDetail.leftUpper.y - _maxRectDetail.rightUpper.y) * (_maxRectDetail.leftUpper.x - _maxRectDetail.rightUpper.x) /
			(_maxRectDetail.leftUpper.x - _maxRectDetail.rightUpper.x) + _maxRectDetail.rightUpper.y;
	}
	else
	{
		_maxRectDetail.leftLower.x = _maxRectDetail.leftUpper.x;
		_maxRectDetail.leftLower.y = (_maxRectDetail.leftLower.y - _maxRectDetail.rightLower.y) * (_maxRectDetail.leftLower.x - _maxRectDetail.rightLower.x) /
			(_maxRectDetail.leftLower.x - _maxRectDetail.rightLower.x) + _maxRectDetail.rightLower.y;
	}
	if (_maxRectDetail.rightLower.x < _maxRectDetail.rightUpper.x)
	{
		_maxRectDetail.rightLower.x = _maxRectDetail.rightUpper.x;
		_maxRectDetail.rightLower.y = (_maxRectDetail.leftLower.y - _maxRectDetail.rightLower.y) * (_maxRectDetail.rightLower.x - _maxRectDetail.rightLower.x) /
			(_maxRectDetail.leftLower.x - _maxRectDetail.rightLower.x) + _maxRectDetail.rightLower.y;
	}
	else
	{
		_maxRectDetail.rightUpper.x = _maxRectDetail.rightLower.x;
		_maxRectDetail.rightUpper.y = (_maxRectDetail.leftUpper.y - _maxRectDetail.rightUpper.y) * (_maxRectDetail.rightUpper.x - _maxRectDetail.rightUpper.x) /
			(_maxRectDetail.leftUpper.x - _maxRectDetail.rightUpper.x) + _maxRectDetail.rightUpper.y;

	}
	//垂足点确认
	_maxRectDetail.centroidUpperLine = cv::Point2f((_maxRectDetail.rightUpper.x + _maxRectDetail.leftUpper.x) / 2,
		(_maxRectDetail.rightUpper.y + _maxRectDetail.leftUpper.y) / 2);
	//下边的线斜率
	float lowerSlope = 0;
	//与下边线垂直的斜率
	float verticalSlope = 0;
	//要把矩形上边和x轴平行的情况去掉
	if (_maxRectDetail.rightUpper.y - _maxRectDetail.leftUpper.y == 0)
		_maxRectDetail.centroidLowerLine = cv::Point2f((_maxRectDetail.rightLower.x + _maxRectDetail.leftLower.x) / 2,
			(_maxRectDetail.rightLower.y + _maxRectDetail.leftLower.y) / 2);
	else
	{
		//k2(x-x2)+y2=k1(x-x3)+y3[o
		//求出下边的斜率
		 lowerSlope = (_maxRectDetail.rightLower.y - _maxRectDetail.leftLower.y) / (_maxRectDetail.rightLower.x - _maxRectDetail.leftLower.x);
		 verticalSlope = (-1) / lowerSlope;
		_maxRectDetail.centroidLowerLine.x = (lowerSlope * _maxRectDetail.rightLower.x - _maxRectDetail.rightLower.y + _maxRectDetail.centroidUpperLine.y - verticalSlope * _maxRectDetail.centroidUpperLine.x) /
			(lowerSlope - verticalSlope);
		_maxRectDetail.centroidLowerLine.y = lowerSlope * (_maxRectDetail.centroidLowerLine.x - _maxRectDetail.rightLower.x) + _maxRectDetail.rightLower.y;
	}
	//轮廓如果占比过高，说明是个短波，需要逼近处理
	//上面的线下平移，下面的线上平移动
	if (cv::contourArea(_maxContours[0]) / (double)_maxContourRect.size.area() > 0.5 /*TRUE*/)
	{
		int emptyArea = 0;
		float rectArea = 0;
		std::vector<int> upperEmptyArea;
		std::vector<int> lowerEmptyArea;
		RectDetail originRect = _maxRectDetail;
		//上边线
		for (float x = originRect.leftUpper.x; x <= originRect.rightUpper.x; x++)
		{
			// y = k(x-x1)+y1
			//沿着y轴向下遍历，碰到中间的线就停止
			for (float y = lowerSlope * (x - originRect.leftUpper.x) + originRect.leftUpper.y;
				y <= lowerSlope * (x - (originRect.leftUpper.x + originRect.leftLower.x) / 2) + (originRect.leftUpper.y + originRect.leftUpper.y) / 2;
				y++)
			{
				if (_contoursMat.at<uchar>(cv::Point(x, y)) == 0)
					emptyArea++;
				rectArea++;
			}
			upperEmptyArea.push_back(emptyArea);
			emptyArea = 0;
		}
		std::sort(upperEmptyArea.begin(), upperEmptyArea.end(), [](const int& a, const int& b) 
			{
				return a < b;
			});
		if (*upperEmptyArea.begin() <= 2)
		{
			//如果太多小于2的，就在15%个数里面求平均值
			int offset = 0;
			for (int i = 0; i < static_cast<int>(static_cast<float>(upperEmptyArea.size()) * 0.15); i++)
				offset += upperEmptyArea[i];
			offset = offset / static_cast<int>(static_cast<float>(upperEmptyArea.size()) * 0.15);

			_maxRectDetail.leftUpper.y += offset;
			_maxRectDetail.rightUpper.y += offset;
			_maxRectDetail.centroidUpperLine.y += offset;
		}
		else
		{
			_maxRectDetail.leftUpper.y += *upperEmptyArea.begin();
			_maxRectDetail.rightUpper.y += *upperEmptyArea.begin();
			_maxRectDetail.centroidUpperLine.y += *upperEmptyArea.begin();
		}

		emptyArea = 0;
		rectArea = 0;
		//下边线
		for (float x = originRect.leftLower.x; x <= originRect.rightLower.x; x++)
		{
			// y = k(x-x1)+y1
			//沿着y轴向上遍历，碰到中间的线就停止
			for (float y = lowerSlope * (x - originRect.leftLower.x) + originRect.leftLower.y;
				y >= lowerSlope * (x - (originRect.leftUpper.x + originRect.leftLower.x) / 2) + (originRect.leftUpper.y + originRect.leftUpper.y) / 2;
				y--)
			{
				if (_contoursMat.at<uchar>(cv::Point(x, y)) == 0)
					emptyArea++;
				rectArea++;
			}
			lowerEmptyArea.push_back(emptyArea);
			emptyArea = 0;
		}
		std::sort(lowerEmptyArea.begin(), lowerEmptyArea.end(), [](const int& a, const int& b)
			{
				return a < b;
			});
		if (*lowerEmptyArea.begin() <= 2)
		{
			//如果太多小于2的，就在15%个数里面求平均值
			int offset = 0;
			for (int i = 0; i < static_cast<int>(static_cast<float>(lowerEmptyArea.size()) * 0.15); i++)
				offset += lowerEmptyArea[i];
			offset = offset / static_cast<int>(static_cast<float>(lowerEmptyArea.size()) * 0.15);

			_maxRectDetail.leftLower.y -= offset;
			_maxRectDetail.rightLower.y -= offset;
			_maxRectDetail.centroidLowerLine.y -= offset;
		}
		else
		{
			_maxRectDetail.leftLower.y -= *lowerEmptyArea.begin();
			_maxRectDetail.rightLower.y -= *lowerEmptyArea.begin();
			_maxRectDetail.centroidLowerLine.y -= *lowerEmptyArea.begin();
		}
	}
}

void MarkGraph::DrawLineMark()
{
	//标注峰峰值上下线
	cv::line(_srcMat, PointRound(_maxRectDetail.leftUpper), PointRound(_maxRectDetail.rightUpper), cv::Scalar(0, 0, 255), 1, cv::LINE_AA, 0);
	cv::line(_srcMat, PointRound(_maxRectDetail.leftLower), PointRound(_maxRectDetail.rightLower), cv::Scalar(0, 0, 255), 1, cv::LINE_AA, 0);
	cv::line(_srcMat, PointRound(_maxRectDetail.centroidUpperLine), PointRound(_maxRectDetail.centroidLowerLine), cv::Scalar(0, 0, 255), 1, cv::LINE_AA, 0);

	//12.5标注
	cv::line(_srcMat, cv::Point2f(26, 67), cv::Point2f(51, 67), cv::Scalar(0, 0, 255), 1, cv::LINE_AA, 0);
	cv::line(_srcMat, cv::Point2f(34, 67), cv::Point2f(34, 117), cv::Scalar(0, 0, 255), 1, cv::LINE_AA, 0);
	cv::line(_srcMat, cv::Point2f(26, 117), cv::Point2f(51, 117), cv::Scalar(0, 0, 255), 1, cv::LINE_AA, 0);

	cv::Mat textMark = cv::Mat::zeros(50, 50, CV_8UC3);//列 行 y x
	//textMark.setTo(cv::Scalar(0, 0, 0));
	//掩码图
	//cv::Mat textMarkMask = cv::Mat::zeros(50, 50, CV_8UC3);
	//textMarkMask.setTo(cv::Scalar(0, 0, 0));
	cv::putText(textMark, "12.5", cv::Point2f(5, 46), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 255), 1);
	//设置旋转矩阵，绕图片中点旋转
	cv::Mat textMarkCal = cv::getRotationMatrix2D(cv::Point2f(textMark.cols / 2, textMark.rows / 2), -90, 1);
	//应用旋转
	cv::warpAffine(textMark, textMark, textMarkCal, textMark.size(), cv::INTER_LANCZOS4);
	//画布设置
	cv::Rect roiRect = cv::Rect(34, 67, textMark.cols, textMark.rows);
	/*cv::cvtColor(srcImage, srcImage, cv::COLOR_BGR2BGRA);*/
	textMark.copyTo(_srcMat(roiRect), textMark);
	/*cv::cvtColor(srcImage, srcImage, cv::COLOR_BGRA2BGR);*/
}

void MarkGraph::DrawPeakLine()
{
	//标记峰峰值
	//计算两标记线的垂线的斜率
	float markLineSlope = (_maxRectDetail.centroidUpperLine.y - _maxRectDetail.centroidLowerLine.y) /
		(_maxRectDetail.centroidUpperLine.x - _maxRectDetail.centroidLowerLine.x);
	//计算峰峰值
	_markPeakDist = GetDistance(_maxRectDetail.centroidLowerLine, _maxRectDetail.centroidUpperLine);
	//峰峰值转图示的值
	_markPeakDist = _markPeakDist * DefineRealDist / DefinePxDist;
	std::string markLineDistStr;
	std::stringstream markLineDistStream;
	//保留两位小数
	markLineDistStream << std::setiosflags(std::ios::fixed) << std::setprecision(2) << _markPeakDist;
	markLineDistStr = markLineDistStream.str();
	//                    col   x
	//   X------------------------+   +------------+-----------+
	//   |XX                          |            |           |
	//   |  X                         |            |           |
	//   |   XXX        斜率为正      |            |           |
	//   |     XXX                    |            |           |
	//   |       XX                   |            |        x  |
	//   |        XXX                 +------------------------+
	// r |          XX                |            |原点       |
	// o |           XXX              |            |           |
	// w |             XX             |            |           |
	//   |              XX            |            |           |
	// y +                            |           y|           |
	//                                +------------------------+
	//                                             

	//斜率大于等于0
	if (markLineSlope >= 0)
	{
		//长度数字标识
		cv::Mat peakText = cv::Mat::zeros(160, 160, CV_8UC3);
		// cv::Mat peakTextMask = cv::Mat::zeros(peakText.rows, peakText.cols, CV_8UC3);
		cv::Point2f OriginPot(peakText.cols / 2, peakText.rows / 2);
		cv::Point2f textLoc;
		//如果斜率是小于1（小于45deg），那要另外处理
		if (markLineSlope < 1)
		{
			textLoc = cv::Point2f(OriginPot.x + peakText.cols / 2 / 4,
				OriginPot.y + markLineSlope * peakText.cols / 2 / 4);//已知x求y
		}
		else
		{
			textLoc = cv::Point2f(OriginPot.x + peakText.rows / 2 / 4 / markLineSlope,
				OriginPot.y + peakText.rows / 2 / 4);//已知y求x

		}
		cv::putText(peakText, markLineDistStr, textLoc, cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 255), 1);
		//设置旋转矩阵，绕字体左下角点旋转
		double rotDeg = (-1) * std::atan(markLineSlope) * (180 / PI);
		cv::Mat peakTextCal = cv::getRotationMatrix2D(cv::Point2f(textLoc.x + 4, textLoc.y), rotDeg, 1);
		//旋转函数
		cv::warpAffine(peakText, peakText, peakTextCal, peakText.size(), cv::INTER_LANCZOS4);
		//画延长线
		cv::line(peakText, PointRound(OriginPot), PointRound(cv::Point2f(OriginPot.x + ((float)peakText.rows / markLineSlope), OriginPot.y + (float)peakText.rows)), cv::Scalar(0, 0, 255), 1, cv::LINE_AA, 0);

		//原图像应用标识
		cv::Rect roiRect_ = cv::Rect(cvRound(_maxRectDetail.centroidLowerLine.x - OriginPot.x), cvRound(_maxRectDetail.centroidLowerLine.y - OriginPot.y), peakText.cols, peakText.rows);
		peakText.copyTo(_srcMat(roiRect_), peakText);
	}
	else
	{
		//斜率小于0
		//长度数字标识
		cv::Mat peakText = cv::Mat::zeros(160, 160, CV_8UC3);
		// cv::Mat peakTextMask = cv::Mat::zeros(peakText.rows, peakText.cols, CV_8UC3);
		cv::Point2f OriginPot(peakText.cols / 2, peakText.rows / 2);
		cv::Point2f textLoc;
		//如果斜率是大于-1（小于45deg），那要另外处理
		if (markLineSlope > -1)
		{
			textLoc = cv::Point2f(OriginPot.x + (peakText.cols / 2 - peakText.cols / 2 / 4),
				OriginPot.y + markLineSlope * (peakText.cols / 2 - peakText.cols / 2 / 4));//已知x求y
		}
		else
		{
			textLoc = cv::Point2f(OriginPot.x + (peakText.rows / 2 - peakText.rows / 2 / 4) / markLineSlope,
				OriginPot.y + (peakText.rows / 2 - peakText.rows / 2 / 4));//已知y求x
		}
		cv::putText(peakText, markLineDistStr, textLoc, cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 255), 1);
		//设置旋转矩阵，绕字体左下角点旋转
		double rotDeg = std::atan((-1) * markLineSlope) * (180 / PI);
		cv::Mat peakTextCal = cv::getRotationMatrix2D(cv::Point2f(textLoc.x - 4, textLoc.y), rotDeg, 1);
		cv::warpAffine(peakText, peakText, peakTextCal, peakText.size(), cv::INTER_LANCZOS4);

		//画延长线
		cv::line(peakText, PointRound(OriginPot), PointRound(cv::Point2f(OriginPot.x + ((float)peakText.rows / markLineSlope), OriginPot.y + (float)peakText.rows)), cv::Scalar(0, 0, 255), 1, cv::LINE_AA, 0);

		//原图像应用标识
		cv::Rect roiRect_ = cv::Rect(cvRound(_maxRectDetail.centroidLowerLine.x - OriginPot.x), cvRound(_maxRectDetail.centroidLowerLine.y - OriginPot.y), peakText.cols, peakText.rows);
		peakText.copyTo(_srcMat(roiRect_), peakText);
	}
}
