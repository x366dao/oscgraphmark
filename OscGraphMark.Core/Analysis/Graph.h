#ifndef GRAPH_H
#define GRAPH_H

#include "..\pch.h"
#include "..\CalcGraph.h"
#include "opencv2/opencv.hpp"
#include "opencv2/core/cvdef.h"
#include "opencv2/highgui.hpp"
#include "opencv2/features2d.hpp"

/// <summary>
/// ͼ��������
/// </summary>
class Graph
{
public:
	//explicit Graph();
	virtual cv::Mat GetMat() = NULL;

protected:
	cv::Mat _srcMat;
	cv::Mat _dstMat;
};

#endif // !GRAPH_H
