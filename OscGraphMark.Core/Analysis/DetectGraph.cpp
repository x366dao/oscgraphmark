#include "DetectGraph.h"

DetectGraph::DetectGraph(const cv::Mat& srcMat)
{
	_srcMat = srcMat.clone();
}

DetectGraph::~DetectGraph(){}

cv::Mat DetectGraph::GetMat()
{
    FindMarkLine();
    ProcessMat();
    FindContour();
    _dstMat = _srcMat.clone();
	return _dstMat;
}

int DetectGraph::GetLeftMark()
{
	return _leftMark;
}

int DetectGraph::GetRightMark()
{
	return _rightMark;
}

std::vector<std::vector<cv::Point>> DetectGraph::GetTargetContours()
{
    return _maxContours;
}

void DetectGraph::FindMarkLine()
{
	//找垂直的两个线
	std::vector<VertialPixel> vertialPixelVector;
	for (int x = 0; x < _srcMat.cols; x++)
	{
		VertialPixel vp = {
			x,
			0
		};

		for (int y = 0; y < _srcMat.rows; y++)
		{

			if (_srcMat.at<uchar>(cv::Point(x, y)) == 255)
			{
				vp.pixelCount++;
			}
		}
		vertialPixelVector.push_back(vp);
	}
    std::sort(vertialPixelVector.begin(), vertialPixelVector.end(), [](const VertialPixel& a, const VertialPixel& b)
		{
			return a.pixelCount > b.pixelCount;
		});
	if (vertialPixelVector.capacity() > 2)
	{
		if (vertialPixelVector[0].pixelVertical < vertialPixelVector[1].pixelVertical)
		{
			//标记线1在左边，标记线0在右边
			_leftMark = vertialPixelVector[0].pixelVertical;
			_rightMark = vertialPixelVector[1].pixelVertical;
		}
		else if (vertialPixelVector[1].pixelVertical < vertialPixelVector[0].pixelVertical)
		{
			//标记线0在左边，标记线1在右边
			_leftMark = vertialPixelVector[1].pixelVertical;
			_rightMark = vertialPixelVector[0].pixelVertical;
		}
	}
}

void DetectGraph::ProcessMat()
{
	//直接把竖着的标记线左边和右边的图像去掉
	if ((static_cast<float>(_rightMark - _leftMark) / static_cast<float>(_srcMat.cols)) < 0.3)
	{
		cv::rectangle(_srcMat, cv::Point(0, 0), cv::Point(_leftMark /*- 10*/, _srcMat.rows), cv::Scalar(0), -1, 4);
		cv::rectangle(_srcMat, cv::Point(_rightMark /*+ 10*/, 0), cv::Point(_srcMat.cols, _srcMat.rows), cv::Scalar(0), -1, 4);
	}
	else
	{
		cv::rectangle(_srcMat, cv::Point(0, 0), cv::Point(_leftMark - 10, _srcMat.rows), cv::Scalar(0), -1, 4);
		cv::rectangle(_srcMat, cv::Point(_rightMark + 10, 0), cv::Point(_srcMat.cols, _srcMat.rows), cv::Scalar(0), -1, 4);
	}
	//去除标没用的标记线
    cv::Mat crossKernel;
    //先腐蚀，去除大量无效点
	 crossKernel = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(2, 3));
	cv::morphologyEx(_srcMat, _srcMat, cv::MORPH_ERODE, crossKernel);

    crossKernel = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(3, 2));
	cv::morphologyEx(_srcMat, _srcMat, cv::MORPH_DILATE, crossKernel);

	crossKernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(6, 3));
	cv::morphologyEx(_srcMat, _srcMat, cv::MORPH_CLOSE, crossKernel);

	//闭运算恢复部分点
	crossKernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 6));
	cv::morphologyEx(_srcMat, _srcMat, cv::MORPH_CLOSE, crossKernel);
}

void DetectGraph::FindContour()
{
    //找出图中的轮廓
    std::vector<std::vector<cv::Point> > contours;
    
    std::vector<std::vector<cv::Point> > maxContours;
    cv::findContours(_srcMat, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE);

    //for (std::vector<std::vector<cv::Point> >::size_type i = 0; i != contours.size();)
    for (std::vector<std::vector<cv::Point>>::iterator iter = contours.begin(); iter != contours.end(); )
    {
        //可能要写函数去除一些不要的轮廓
        if (cv::contourArea(*iter) > 5 * 5)
        {
            //剔除最大的轮廓
            maxContours.push_back(*iter);
            //contours.erase(contours.begin()+i);
            iter = contours.erase(iter);
        }
        else
        {
            iter++;
        }
        /*
        else
        {
            i++;
        }*/
    }
    //只要其中最大的轮廓
    if (maxContours.size() > 1)
    {
        std::vector<cv::Point> maxArea;
        std::vector<std::vector<cv::Point> >::iterator maxIter;
        for (std::vector<std::vector<cv::Point> >::iterator iter = maxContours.begin(); iter != maxContours.end(); )
        {
            if (iter == maxContours.begin())
            {
                maxArea = *iter;
                //获取当前的迭代器
                maxIter = iter;
                iter++;
                continue;
            }
            //第i+1的轮廓和刚刚的对比，如果出现小于的情况，就剔除
            if (cv::contourArea(*iter) <= cv::contourArea(maxArea))
            {
                //可能出现等于的情况
                //maxArea = maxContours[i];
                contours.push_back(*iter);
                iter = maxContours.erase(iter);
            }
            else
            {
                //如果是大于的情况，就将小的剔除
                contours.push_back(maxArea);
                //获得新的最大面积
                maxArea = *iter;
                iter = maxContours.erase(maxIter);
                maxIter = iter;
            }
            //遍历到最后一个的时候
            if (iter == maxContours.end())
            {
                if (maxContours.size() < 1)
                    maxContours.push_back(maxArea);
                continue;
            }
        }
    }
    _maxContours = maxContours;
    //只展示最大的轮廓
    cv::drawContours(_srcMat, contours, -1, cv::Scalar(0), -1);
}
