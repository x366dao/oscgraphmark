#ifndef PREPROCESSGRAPH_H
#define PREPROCESSGRAPH_H

#include "Graph.h"

#define R_CHANNEL 2

//using namespace Analysis;


class PreprocessGraph :
	public Graph
{
public:
	/// <summary>
	/// 预处理
	/// </summary>
	explicit PreprocessGraph(const cv::Mat& srcMat);
	~PreprocessGraph();
	
	cv::Mat GetMat() override;

private:
	std::vector<cv::Point> _backgroundPoint;
	cv::Mat _rChannelImage;

	/// <summary>
	/// 去除图像存在大量干扰的区域
	/// </summary>
	void RemoveUnlessArea();

	/// <summary>
	/// 提取图片中的背景所在的点坐标
	/// </summary>
	void FindBackgroundPoint();

	/// <summary>
	/// 二值化图片
	/// </summary>
	/// <param name="srcMat">原图</param>
	/// <param name="dstMat">目标图</param>
	/// <param name="threshold">界限值</param>
	/// <param name="dstVal">转换值</param>
	void BinThreshold(const cv::Mat& srcMat, cv::Mat& dstMat, int threshold, int dstVal);

	/// <summary>
	/// 去除目标图像中的背景
	/// </summary>
	void RemoveBackground();
};
#endif // !PREPROCESSGRAPH_H