#include "_exports.h"

SYMBOL_DECLSPEC int __stdcall GetGraphMark(
	_In_  FileBuffer* pInBuffer,
	_Out_ FileBuffer* pOutBuffer,
	_Out_ float* peakMarkVal
)
{
	try
	{
		//这里的指针由调用者释放
		cv::Mat src;
		ConvertByteToMat(pInBuffer->pBuffer, src, pInBuffer->size);

		//原图备份
		cv::Mat dst = src.clone();

		//预处理
		PreprocessGraph pg(src);
		src = pg.GetMat();

		//检测
		DetectGraph dg(src);
		src = dg.GetMat();

		//标记
		MarkGraph mg(dst, src,dg.GetTargetContours());
		dst = mg.GetMat();
		*peakMarkVal = mg.GetPeakDist();

		ConvertMatToByte(dst, pOutBuffer->pBuffer, pOutBuffer->size);

#ifndef _DEBUG

#endif // _DEBUG

	}
	catch (const std::exception&)
	{
		return -1;
	}
	return 0;
}
