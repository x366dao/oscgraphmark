#ifndef CALCGRAPH_h
#define CALCGRAPH_h

#include "pch.h"
#include "opencv2/opencv.hpp"
#include "opencv2/core/cvdef.h"
#include "opencv2/highgui.hpp"
#include "opencv2/features2d.hpp"

/// <summary>
/// 计算两点之间的长度
/// </summary>
/// <param name="pt1"></param>
/// <param name="pt2"></param>
/// <returns></returns>
float GetDistance(cv::Point2f pt1, cv::Point2f pt2);

/// <summary>
/// 将点四舍五入
/// </summary>
/// <param name="pt"></param>
/// <returns></returns>
cv::Point2f PointRound(cv::Point2f pt);

#endif // !CALCGRAPH_h
